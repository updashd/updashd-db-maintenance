<?php
namespace Updashd;

use Predis\Client;
use Updashd\DbEvent\PopulateStatement;
use Updashd\DbEvent\PopulateStatements;
use Updashd\DbEvent\Processor;
use Updashd\DbEvent\Subscriptions;
use Updashd\Doctrine\UTCDateTimeType;
use Updashd\Model\ZzAuditAccount;
use Updashd\Model\ZzAuditAccountNotifier;
use Updashd\Model\ZzAuditAccountPerson;
use Updashd\Model\ZzAuditEnvironment;
use Updashd\Model\ZzAuditIncident;
use Updashd\Model\ZzAuditIncidentNotifierHistory;
use Updashd\Model\ZzAuditMetricType;
use Updashd\Model\ZzAuditNode;
use Updashd\Model\ZzAuditNodeService;
use Updashd\Model\ZzAuditNodeServiceNotifier;
use Updashd\Model\ZzAuditNodeServiceZone;
use Updashd\Model\ZzAuditNotifier;
use Updashd\Model\ZzAuditPerson;
use Updashd\Model\ZzAuditPersonSetting;
use Updashd\Model\ZzAuditResult;
use Updashd\Model\ZzAuditResultMetric;
use Updashd\Model\ZzAuditService;
use Updashd\Model\ZzAuditServiceMetricField;
use Updashd\Model\ZzAuditSetting;
use Updashd\Model\ZzAuditSeverity;
use Updashd\Model\ZzAuditZone;
use Updashd\Scheduler\Model\Task;
use Updashd\Scheduler\Scheduler;
use Updashd\Scheduler\ScheduleType;
use Updashd\Scheduler\State;
use Updashd\Configlib\Config;

class DbCleanup {
    private $config;
    private $doctrineManager;

    public function __construct ($config) {
        // Store the configuration
        $this->setConfig($config);

        $doctrineManager = new Doctrine\Manager($config['doctrine']);
        $this->setDoctrineManager($doctrineManager);
    }


    /**
     * Run the worker
     * @param $running
     */
    public function run (&$running = true) {
        $sleepTime = (int) $this->getConfig('frequency');

        while ($running) {
            printf("Purging event tables...\n");
            $count = $this->purgeEventTables();
            printf("%s events purged.\n", $count);

            sleep($sleepTime);
        }
    }

    protected function purgeEventTables () {
        $tables = [
            ZzAuditAccount::class,
            ZzAuditAccountNotifier::class,
            ZzAuditAccountPerson::class,
            ZzAuditEnvironment::class,
            ZzAuditIncident::class,
            ZzAuditIncidentNotifierHistory::class,
            ZzAuditMetricType::class,
            ZzAuditNode::class,
            ZzAuditNodeService::class,
            ZzAuditNodeServiceNotifier::class,
            ZzAuditNodeServiceZone::class,
            ZzAuditNotifier::class,
            ZzAuditPerson::class,
            ZzAuditPersonSetting::class,
            ZzAuditResult::class,
            ZzAuditResultMetric::class,
            ZzAuditService::class,
            ZzAuditServiceMetricField::class,
            ZzAuditSetting::class,
            ZzAuditSeverity::class,
            ZzAuditZone::class
        ];

        $count = 0;

        foreach ($tables as $table) {
            $count += $this->purgeEventTable($table);
        }

        return $count;
    }

    protected function purgeEventTable ($modelClass) {
        printf("  %s", $modelClass);

        $maxDate =  new \DateTime();
        $maxDate->modify('-1 hour');

        $em = $this->getDoctrineManager()->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb
            ->delete()
            ->from($modelClass, 'm')
            ->where($qb->expr()->lte('m.eventDate', ':date'));

        $q = $qb->getQuery();
        $q->setParameter('date', $maxDate);
        $count = $q->execute();

        printf(" (%s)\n", $count);

        return $count;
    }

    protected function getPdo () {
        $em = $this->getDoctrineManager()->getEntityManager();
        return $em->getConnection();
    }

    /**
     * Retrieve the config, or a key from it.
     * @param string $key
     * @return array
     */
    public function getConfig ($key = null) {
        if ($key && array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }

        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }

    /**
     * @return Doctrine\Manager
     */
    public function getDoctrineManager () {
        return $this->doctrineManager;
    }

    /**
     * @param Doctrine\Manager $doctrineManager
     */
    public function setDoctrineManager ($doctrineManager) {
        $this->doctrineManager = $doctrineManager;
    }
}